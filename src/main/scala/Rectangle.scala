object Rectangle:

  // Functions to manipule rectangle represented by a tuple (x,y), where
  // x and y are integers that represent the rectangle's measure in some
  // unit measure (not important for the exercises)

  //An alias
  type Rectangle = (Int, Int)

  def r2String( r : Rectangle): String = ???

  def perimeter( r : Rectangle): Long = ???

  def area( r : Rectangle): Long = ???

  def fPerimeter(x : Rectangle, y : Rectangle) : Boolean =  ???

  def fArea(x : Rectangle, y : Rectangle) : Boolean =  ???

  def sortRectangles (l : List[Rectangle], f:(Rectangle,Rectangle) => Boolean ) : List[Rectangle] = ???

  def greatestTriangle(r : Rectangle): Int = ???

  def numberOfSquares(r:Rectangle, s:Int): Int = ???

  def sumSidesR(l : List[Rectangle]) : (Int, Int) = ???

  def sumSidesJ(l : List[Rectangle]) : (Int, Int) = ???

  def mapByF(l : List[Rectangle], f:  Rectangle => Long):Map[Long, List[(Int, Int)]] =
    ???

  def mapWithUniqueByF(l : List[Rectangle], f:  Rectangle => Long):Map[Long, List[(Int, Int)]] =
    ???





